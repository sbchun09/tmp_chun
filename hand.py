import os, sys
import subprocess


filename = ''
filetype = ''

print(sys.argv)

if(len(sys.argv) < 3):
    raise Exception("[EEEEE] Input file not provided.")

filename = sys.argv[1]
typeof = sys.argv[2]
filetype = filename[filename.rfind('.')+1:]


if filename == '' or filetype == '':
    raise Exception("[EEEEE] No input file found.")

# move video file to engine directory
nameonly = filename[:filename.rfind('.')]
output_filename = nameonly + '_hand.mp4'

command = ('python detect_single_threaded.py --input_video ./uploads/' + filename +
  ' --output_video output/' + output_filename)

print('command: ', command)

subprocess.run(command.split(), stderr=sys.stderr, stdout=sys.stdout)

if not os.path.isfile('output/' + output_filename):
    raise Exception('[EEEEE] Video is not worked completely...')

# move output file to server directory
print('SUCCESS PROCESSING')