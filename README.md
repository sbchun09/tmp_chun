Blur Face Video Server
=================
NodeJS + Express + EJS server for blurring face & hand from video and image

Contents
-----------------
- 1. Environment Setup
- 2. Run
- 3. Request and Response

Environment Setup
-----------------
1. Clone this repo, `git clone https://gitlab.com/cn-admin/face_blurring_hand_detection.git <your project name>`
2. `cd <your project name>`
3. Remove `.git` folder, `rm -rf .git`
4. Run `yarn` to install dependencies
6. Run `mkdir uploads`
7. Run `mkdir output`
8. Start the packager with `yarn dev`