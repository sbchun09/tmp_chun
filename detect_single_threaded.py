from utils import detector_utils as detector_utils
import cv2
import tensorflow as tf
import datetime
import argparse
import os

detection_graph, sess = detector_utils.load_inference_graph()

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-sth',
        '--scorethreshold',
        dest='score_thresh',
        type=float,
        default=0.2,
        help='Score threshold for displaying bounding boxes')
    parser.add_argument(
        '-fps',
        '--fps',
        dest='fps',
        type=int,
        default=1,
        help='Show FPS on detection/display visualization')
    # parser.add_argument(
    #     '-src',
    #     '--source',
    #     dest='video_source',
    #     default=0,
    #     help='Device index of the camera.')
    parser.add_argument(
        '-wd',
        '--width',
        dest='width',
        type=int,
        default=320,
        help='Width of the frames in the video stream.')
    parser.add_argument(
        '-ht',
        '--height',
        dest='height',
        type=int,
        default=180,
        help='Height of the frames in the video stream.')
    parser.add_argument(
        '-ds',
        '--display',
        dest='display',
        type=int,
        default=1,
        help='Display the detected images using OpenCV. This reduces FPS')
    parser.add_argument(
        '-num-w',
        '--num-workers',
        dest='num_workers',
        type=int,
        default=4,
        help='Number of workers.')
    parser.add_argument(
        '-q-size',
        '--queue-size',
        dest='queue_size',
        type=int,
        default=5,
        help='Size of the queue.')
    parser.add_argument('-i',
                        '--input_video',
                        dest= 'video_source' ,
                        help='Path to your video',
                        type=str,
                        required=True)
    parser.add_argument('-o',
                        '--output_video',
                        dest='output_video',
                        help='Output file path',
                        type=str)
    # video_source
    args = parser.parse_args()
    

     # if input image path is invalid then stop
    assert os.path.isfile(args.video_source), 'Invalid input file'

    # if output directory is invalid then stop
    if args.output_video:
        assert os.path.isdir(os.path.dirname(
            args.output_video)), 'No such directory'


    cap = cv2.VideoCapture(args.video_source)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, args.width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, args.height)
    
    my_fps = cap.get(cv2.CAP_PROP_FPS)    

    start_time = datetime.datetime.now()
    num_frames = 0
    im_width, im_height = (cap.get(3), cap.get(4))
    
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    #change frame rate
    output = cv2.VideoWriter(args.output_video,fourcc,10.0,(int(im_width),int(im_height)))


    # max number of hands we want to detect/track
    num_hands_detect = 2

    # cv2.namedWindow('Single-Threaded Detection', cv2.WINDOW_NORMAL)

    while True:
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        ret, image_np = cap.read()
        # print('processing')
        # image_np = cv2.flip(image_np, 1)

        if image_np is None:
            break
        key = cv2.waitKey(1)
        if key & 0xFF == ord('q'):
            break

        try:
            image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
        except:
            print("Error converting to RGB")

        # Actual detection. Variable boxes contains the bounding box cordinates for hands detected,
        # while scores contains the confidence for each of these boxes.
        # Hint: If len(boxes) > 1 , you may assume you have found atleast one hand (within your score threshold)

        boxes, scores = detector_utils.detect_objects(image_np,
                                                      detection_graph, sess)

        # draw bounding boxes on frame
        detector_utils.draw_box_on_image(num_hands_detect, args.score_thresh,
                                         scores, boxes, im_width, im_height,
                                         image_np)

        # Calculate Frames per second (FPS)
        num_frames += 1
        elapsed_time = (datetime.datetime.now() - start_time).total_seconds()
        fps = num_frames / elapsed_time

        if (args.display > 0):
            # Display FPS on frame
            if (args.fps > 0):
                detector_utils.draw_fps_on_image("FPS : " + str(int(fps)),
                                                 image_np)

           # cv2.imshow('Single-Threaded Detection',
           #            cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR))
            

            if cv2.waitKey(25) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break
        else:
            print("frames processed: ", num_frames, "elapsed time: ",
                  elapsed_time, "fps: ", str(int(fps)))

        try:
            image_np = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)
        except:
            print("Error converting to BGR")
        output.write(image_np)