import { PythonShell } from 'python-shell';

const options_face = {
  mode: 'text',
  pythonPath: 'C:/Users/pc/.conda/envs/mindbam_face/python.exe',
  pythonOptions: ['-u'],
}

const options_hand ={
  mode: 'text',
  pythonPath: 'C:/Users/pc/.conda/envs/mindbam_hand/python.exe',
  pythonOptions: ['-u'],
}

export const pyShell_face = (
  script,
  args,
) => new Promise((resolve, reject) => PythonShell.run(
  script,
  { args, ...options_face },
  (err, results) => {
    if(err) reject(err);
    resolve(results);
  }
));
export const pyShell_hand = (
  script,
  args,
) => new Promise((resolve, reject) => PythonShell.run(
  script,
  { args, ...options_hand },
  (err, results) => {
    if(err) reject(err);
    resolve(results);
  }
));
