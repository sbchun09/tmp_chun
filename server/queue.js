import Queue from 'bee-queue';
import redis from 'redis';
import env from 'dotenv';
import { pyShell_face } from './bash';

env.config();
// create redis server
const client = redis.createClient(process.env.REDIS_URL, { password: process.env.REDIS_PWD });

client.on('error', err => console.error(`Error: ${err}`))

const queue = new Queue('video', {
  redis: client,
});
queue.process(async (job, done) => {
  try{
    job.reportProgress(10);
    console.log(`Processing job ${job.data.filename}`);
    const data = await pyShell_face('face.py', [job.data.filename, 'video']);
    return done(null, { filename: job.data.filename });
  } catch(err) {
    console.error('Error:', err);
  }
}
);

export default queue;
// module.exports = {queue,queue_hand};