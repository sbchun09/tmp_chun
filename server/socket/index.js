import SocketIO from 'socket.io';
import SocketUploader from 'socketio-file-upload';
import queue from '../queue';
import path from 'path';
import queue_hand from '../queue_hand';

const io = SocketIO();

io.on('connection', socket => {
  console.log(`socket id[${socket.id}] connected`);
  const uploader = new SocketUploader();

  uploader.dir = path.join(__dirname, '../../uploads');
  uploader.listen(socket);

  uploader.on('start', e => {
    if (/\.exe$/.test(e.file.name)) {
        uploader.abort(e.file.id, socket);
    }
  });
  uploader.on('saved', e => {
    const uploaded = e.file;
    const filetype = uploaded.name.substring(uploaded.name.lastIndexOf(".") + 1);
    const filename = uploaded.base + '.' + filetype;

    e.file.clientDetail.filename = filename;
  });
  uploader.on('error', e => {
    console.log('Error from uploader', e);
  });
  
  
  socket.on('hand start', async filename => {
    console.log('hand start started')
  
    if(!filename){
      return;
    }

    const hand_job = await queue_hand.createJob({
      filename,
      type: 'hand'
    }).save();

    hand_job.on('succeeded',res=>{
      // console.log(res)
      console.log('hand succeeded');
      //console.log('blur started')
      
      socket.emit('hand succeeded',filename);
      
      console.log('filename ' + filename)

      //console.log(socket.id)
      //io.to(socket.id).emit('blur start', { filename, type: 'video' });

      
    
     });
  
    socket.emit('hand start',filename);
    
   
 
  })



  socket.on('blur start', async filename => {
    
    console.log('blur init ' + filename);
    filename=filename.split('.',1) + '_hand'+filename.substring(filename.lastIndexOf("."));
    console.log('blur filename ' + filename);
    if(!filename){
      return;
    }

    const job = await queue.createJob({
      filename,
      type: 'video',
    }).save();
   
    job.on('blur succeeded', res => {
    console.log(res);
     console.long('blur succeeded');
    //  socket.emit('blur succeeded', filename);
    });
    // const job = await queue.createJob({
    //    filename,
    //    type: 'video',
    //  }).save();
    
    //  job.on('blur succeeded', res => {
    //  console.log(res);
    //   console.long('blur succeeded');
    //   socket.emit('blur succeeded', filename);

    //  });
     //socket.emit('blur start', { filename });
 
    // job.on('progress', progress => {
    //   console.log(progress);
    //   //socket.emit('blur progress', progress);
    // });

    
 
  })
});




export default io;
