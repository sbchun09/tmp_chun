import Queue from 'bee-queue';
import redis from 'redis';
import env from 'dotenv';
import { pyShell_hand } from './bash';

env.config();
// create redis server
const client = redis.createClient(process.env.REDIS_URL, { password: process.env.REDIS_PWD });

client.on('error', err => console.error(`Error: ${err}`))

 const queue_hand = new Queue('hand', {
   redis: client,
 });


queue_hand.process(async (job, done) => {
  try{
    job.reportProgress(10);
    console.log(`Processing hand  ${job.data.filename}`);
    const data = await pyShell_hand('hand.py', [job.data.filename, 'hand']);
    return done(null, { filename: job.data.filename });
    
  } catch(err) {
    console.error('Error:', err);
  }
}
);

export default queue_hand;
