import os, sys
import subprocess
# import glob
# import shutil
# import tqdm
# import math

# initialize directory
"""
if os.path.isdir("MesoNet/Image_input/image"):
    shutil.rmtree("MesoNet/Image_input/image")
if os.path.isdir("MesoNet/Image_output"):
    shutil.rmtree("MesoNet/Image_output")
if os.path.isdir("FaceForensics/classification/Video_input"):
    shutil.rmtree("FaceForensics/classification/Video_input")
if os.path.isdir("FaceForensics/classification/Video_output"):
    shutil.rmtree("FaceForensics/classification/Video_output")
os.mkdir("MesoNet/Image_input/image")
os.mkdir("MesoNet/Image_output")
os.mkdir("FaceForensics/classification/Video_input")
os.mkdir("FaceForensics/classification/Video_output")
"""

filename = ''
filetype = ''

print(sys.argv)
if(len(sys.argv) < 3):
    raise Exception("[EEEEE] Input file not provided.");

filename = sys.argv[1]
typeof = sys.argv[2]
filetype = filename[filename.rfind('.')+1:]

if filename == '' or filetype == '':
    raise Exception("[EEEEE] No input file found.");

if typeof == 'image':
    # something
    print('image not supported')
elif typeof == 'video':
    # move video file to engine directory
    nameonly = filename[:filename.rfind('.')]
    output_filename = nameonly + '_output.mp4'

    command = ('python auto_blur_video.py --input_video ./output/' + filename +
      ' --output_video output/' + output_filename +
      ' --model_path ./face_model/face.pb --threshold 0.7')
    print('command: ', command)
    subprocess.run(command.split(), stderr=sys.stderr, stdout=sys.stdout)

    if not os.path.isfile('output/' + output_filename):
        raise Exception('[EEEEE] Video is not worked completely...')

    # move output file to server directory
    print('SUCCESS PROCESSING')